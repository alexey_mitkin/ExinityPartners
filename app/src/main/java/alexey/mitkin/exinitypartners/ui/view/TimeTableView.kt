package alexey.mitkin.exinitypartners.ui.view

import alexey.mitkin.exinitypartners.R
import alexey.mitkin.exinitypartners.data.entity.Place
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.FrameLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import kotlinx.android.synthetic.main.view_time_table.view.*
import java.util.*

class TimeTableView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private lateinit var place: Place

    val root: View
        get() = rootLayout

    init {
        View.inflate(context, R.layout.view_time_table, this)
    }

    fun updateTimeTable() {
        setupTimeTableForDay(
            place.monday,
            textViewTimeOpenMonday,
            textViewTimeCloseMonday
        )
        setupTimeTableForDay(
            place.tuesday,
            textViewTimeOpenTuesday,
            textViewTimeCloseTuesday
        )
        setupTimeTableForDay(
            place.wednesday,
            textViewTimeOpenWednesday,
            textViewTimeCloseWednesday
        )
        setupTimeTableForDay(
            place.thursday,
            textViewTimeOpenThursday,
            textViewTimeCloseThursday
        )
        setupTimeTableForDay(
            place.friday,
            textViewTimeOpenFriday,
            textViewTimeCloseFriday
        )
        setupTimeTableForDay(
            place.saturday,
            textViewTimeOpenSaturday,
            textViewTimeCloseSaturday
        )
        setupTimeTableForDay(
            place.sunday,
            textViewTimeOpenSunday,
            textViewTimeCloseSunday
        )

        setupCurrentDay()
    }

    private fun setupTimeTableForDay(
        dayOfWeek: String,
        textViewOpen: TextView,
        textViewClose: TextView
    ) {
        if (dayOfWeek == "-") {
            textViewOpen.text = "-"
            return
        }
        val times = dayOfWeek.split("-")
        if (times.size < 2) {
            textViewOpen.text = "-"
            return
        }
        textViewOpen.text = times[0]
        textViewClose.text = times[1]
    }

    private fun setupCurrentDay() {
        when (detectDayOfTheWeek()) {
            Calendar.MONDAY -> highlightCurrentDay(viewMondayStatus, place.monday)
            Calendar.TUESDAY -> highlightCurrentDay(viewTuesdayStatus, place.tuesday)
            Calendar.WEDNESDAY -> highlightCurrentDay(viewWednesdayStatus, place.wednesday)
            Calendar.THURSDAY -> highlightCurrentDay(viewThursdayStatus, place.thursday)
            Calendar.FRIDAY -> highlightCurrentDay(viewFridayStatus, place.friday)
            Calendar.SATURDAY -> highlightCurrentDay(viewSaturdayStatus, place.saturday)
            Calendar.SUNDAY -> highlightCurrentDay(viewMondayStatus, place.sunday)
        }
    }

    private fun highlightCurrentDay(view: FrameLayout, placeTimeString: String) {
        view.apply {
            visibility = View.VISIBLE

            val backgroundResource =
                if (isPlaceOpenNow(placeTimeString))
                    R.drawable.background_time_table_open
                else
                    R.drawable.background_time_table_close

            setBackgroundResource(backgroundResource)
        }
    }

    private fun isPlaceOpenNow(placeTimeString: String): Boolean {
        val calendar = Calendar.getInstance()
        val time = calendar.get(Calendar.HOUR_OF_DAY) * 100 + calendar.get(Calendar.MINUTE)

        if (placeTimeString == "-") return false

        val times = placeTimeString.split("-")
        if (times.size != 2) return false

        val openTimeParts = times[0].split(":")
        val closeTimeParts = times[1].split(":")

        val openTime = openTimeParts[0].toInt() * 100 + openTimeParts[1].toInt()
        val closeTime = closeTimeParts[0].toInt() * 100 + closeTimeParts[1].toInt()

        //for times cases 11:00-02:00 07:00-01:00  14:00 - 05:00 etc.
        if (openTime > closeTime) {
            return if (time in 600..2359)
                time > openTime
            else {
                time < closeTime
            }
        }

        return time in openTime..closeTime
    }

    private fun detectDayOfTheWeek(): Int =
        Calendar.getInstance().get(Calendar.DAY_OF_WEEK)

    companion object {
        @JvmStatic
        @BindingAdapter("app:timetable_place")
        fun setPlace(view: TimeTableView, place: Place) {
            view.place = place
            view.updateTimeTable()
        }
    }
}