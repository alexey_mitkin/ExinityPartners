package alexey.mitkin.exinitypartners.ui

import alexey.mitkin.exinitypartners.R
import alexey.mitkin.exinitypartners.ui.map.MapFragment
import androidx.annotation.ColorRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.*

/**
 * For Actvities, allows declarations like
 * ```
 * val myViewModel = viewModelProvider(myViewModelFactory)
 * ```
 */
inline fun <reified VM : ViewModel> AppCompatActivity.viewModelProvider(provider: ViewModelProvider.Factory) =
    ViewModelProviders.of(this, provider).get(VM::class.java)

/**
 * For Fragments, allows declarations like
 * ```
 * val myViewModel = viewModelProvider(myViewModelFactory)
 * ```
 */
inline fun <reified VM : ViewModel> Fragment.viewModelProvider(provider: ViewModelProvider.Factory) =
    ViewModelProviders.of(this, provider).get(VM::class.java)

fun <T1, T2, R> zip2(
    src1: LiveData<T1>, src2: LiveData<T2>,
    zipper: (T1, T2) -> R
): LiveData<R> {

    return MediatorLiveData<R>().apply {
        var src1Version = 0
        var src2Version = 0

        var lastSrc1: T1? = null
        var lastSrc2: T2? = null

        fun updateValueIfNeeded() {
            if (src1Version > 0 && src2Version > 0 &&
                lastSrc1 != null && lastSrc2 != null
            ) {
                value = zipper(lastSrc1!!, lastSrc2!!)
                src1Version = 0
                src2Version = 0
            }
        }

        addSource(src1) {
            lastSrc1 = it
            src1Version++
            updateValueIfNeeded()
        }

        addSource(src2) {
            lastSrc2 = it
            src2Version++
            updateValueIfNeeded()
        }
    }
}

fun Fragment.getMapFragment(): MapFragment {
    return activity
        ?.supportFragmentManager
        ?.findFragmentById(R.id.nav_host_fragment)
        ?.childFragmentManager
        ?.fragments
        ?.first() as MapFragment
}

fun Fragment.setNavigationBarColor(@ColorRes colorId: Int) {
    activity?.window?.navigationBarColor = ContextCompat.getColor(requireContext(), colorId)
}