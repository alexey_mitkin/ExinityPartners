package alexey.mitkin.exinitypartners.ui.blank

import alexey.mitkin.exinitypartners.di.anotation.FragmentScoped
import alexey.mitkin.exinitypartners.di.data.ViewModelKey
import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class BlankModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeBlankFragment(): BlankFragment

    @Binds
    @IntoMap
    @ViewModelKey(BlankViewModel::class)
    abstract fun bindBlankViewModel(viewModel:BlankViewModel): ViewModel
}