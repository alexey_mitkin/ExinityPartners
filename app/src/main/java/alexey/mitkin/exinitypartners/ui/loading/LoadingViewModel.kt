package alexey.mitkin.exinitypartners.ui.loading

import alexey.mitkin.exinitypartners.AppRepository
import alexey.mitkin.exinitypartners.Event
import alexey.mitkin.exinitypartners.data.Result
import alexey.mitkin.exinitypartners.data.sharedpref.SharedPreferenceContract
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class LoadingViewModel @Inject constructor(
    private val repository: AppRepository,
    private val settings: SharedPreferenceContract
) : ViewModel() {

    private val _navigateToMap = MutableLiveData<Event<Unit>>()
    val navigateToMap: LiveData<Event<Unit>> = _navigateToMap

    private val _navigateToOffline = MutableLiveData<Event<Unit>>()
    val navigateToOffline: LiveData<Event<Unit>> = _navigateToOffline

    private val _showOfflineNotificationToast = MutableLiveData<Event<Unit>>()
    val showOfflineNotificationToast: LiveData<Event<Unit>> = _showOfflineNotificationToast

    fun onFragmentStart() {
        viewModelScope.launch {
            when (val result = repository.fetch()) {
                is Result.Success -> {
                    settings.lastSavedAt = System.currentTimeMillis()
                    _navigateToMap.value = Event(Unit)
                }
                else -> {
                    if (settings.lastSavedAt != 0L) {
                        _showOfflineNotificationToast.value = Event(Unit)
                        _navigateToMap.value = Event(Unit)
                    } else {
                        (result as Result.Error).exception.printStackTrace()
                        _navigateToOffline.value = Event(Unit)
                    }
                }
            }
        }
    }
}