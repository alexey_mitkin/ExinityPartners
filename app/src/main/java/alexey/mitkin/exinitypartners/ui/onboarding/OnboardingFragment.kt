package alexey.mitkin.exinitypartners.ui.onboarding

import alexey.mitkin.exinitypartners.R
import alexey.mitkin.exinitypartners.databinding.FragmentOnboardingBinding
import alexey.mitkin.exinitypartners.ui.base.BaseFragment
import alexey.mitkin.exinitypartners.ui.setNavigationBarColor
import alexey.mitkin.exinitypartners.ui.viewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_onboarding.*
import javax.inject.Inject

class OnboardingFragment : BaseFragment() {

    lateinit var viewModel: OnboardingViewModel

    private lateinit var viewDataBinding: FragmentOnboardingBinding

    @Inject
    lateinit var viewPagerFragmentStateAdapter: ViewPagerFragmentStateAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = viewModelProvider(viewModelFactory)
        viewDataBinding = FragmentOnboardingBinding.inflate(inflater, container, false)
        viewDataBinding.viewmodel = viewModel
        return viewDataBinding.rootLayout
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupViewPager()
        setNavigationBarColor(R.color.exinity_dark)
    }

    private fun setupViewPager() {
        viewPagerOnboarding.adapter = viewPagerFragmentStateAdapter
        circleIndicator.setViewPager(viewPagerOnboarding)
    }


}