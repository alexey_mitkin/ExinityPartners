package alexey.mitkin.exinitypartners.ui.map

import alexey.mitkin.exinitypartners.Event
import alexey.mitkin.exinitypartners.data.entity.Category
import alexey.mitkin.exinitypartners.data.entity.Place
import alexey.mitkin.exinitypartners.data.entity.PlacePartnerCategory
import alexey.mitkin.exinitypartners.data.local.CacheImagesController
import alexey.mitkin.exinitypartners.data.local.ExinityPartnersDatabase
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.coroutines.launch
import javax.inject.Inject

class MapViewModel @Inject constructor(
    private val db: ExinityPartnersDatabase,
    private val cacheImagesController: CacheImagesController
) : ViewModel() {

    private val _markerOptionsItems = MutableLiveData<Event<List<Pair<Place, MarkerOptions>>>>()
    val markerOptionsItems: LiveData<Event<List<Pair<Place, MarkerOptions>>>> = _markerOptionsItems

    private val _showError = MutableLiveData<Event<String>>()
    val showError: LiveData<Event<String>> = _showError

    fun onMapReady() {
        viewModelScope.launch {
            val result = db.placePartnerCategoryDao().getAllPlacePartnerCategory()
            generateListOfMarkers(result)
        }
    }

    private fun generateListOfMarkers(result: List<PlacePartnerCategory>) {
        val list = mutableListOf<Pair<Place, MarkerOptions>>()
        result.forEach {
            if (it.place.coordinates.isEmpty()) return@forEach

            val markerOptions = MarkerOptions()
                .position(it.place.latLng!!) // cause we have this check "if (it.place.coordinates.isEmpty()) return@forEach"
                .title(it.place.name)
                .icon(getBitmapMarker(it.category.id))
            list.add(Pair(it.place, markerOptions))
        }
        _markerOptionsItems.value = Event(list)
    }

    private fun getBitmapMarker(categoryId: String): BitmapDescriptor? {
        return BitmapDescriptorFactory.fromBitmap(
            cacheImagesController.getCategoryMarker(categoryId)
        )
    }

    fun showPlacesByCategory(category: Category?) {
        viewModelScope.launch {
            val result = if (category == null) {
                db.placePartnerCategoryDao().getAllPlacePartnerCategory()
            } else {
                db.placePartnerCategoryDao().getAllPlacePartnerCategoryByCategory(category.id)
            }
            generateListOfMarkers(result)
        }
    }

}