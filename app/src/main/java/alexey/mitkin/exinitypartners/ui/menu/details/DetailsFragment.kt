package alexey.mitkin.exinitypartners.ui.menu.details

import alexey.mitkin.exinitypartners.EventObserver
import alexey.mitkin.exinitypartners.databinding.FragmentCategoryDetailsBinding
import alexey.mitkin.exinitypartners.ui.base.BaseFragment
import alexey.mitkin.exinitypartners.ui.viewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs

class DetailsFragment : BaseFragment() {

    lateinit var viewModel: DetailsViewModel

    private lateinit var viewDataBinding: FragmentCategoryDetailsBinding

    private val args: DetailsFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = viewModelProvider(viewModelFactory)
        viewDataBinding = FragmentCategoryDetailsBinding.inflate(inflater, container, false)
        viewDataBinding.viewmodel = viewModel
        viewDataBinding.lifecycleOwner = this
        viewDataBinding.place = args.place
        return viewDataBinding.rootLayout
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setUpNavigation()
        viewModel.fragmentStart(args.place)
    }

    private fun setUpNavigation() {
        viewModel.navigateBack.observe(viewLifecycleOwner, EventObserver {
            findNavController().popBackStack()
        })
    }
}