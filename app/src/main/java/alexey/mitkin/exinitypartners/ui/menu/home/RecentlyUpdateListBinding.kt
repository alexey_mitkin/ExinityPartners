package alexey.mitkin.exinitypartners.ui.menu.home

import alexey.mitkin.exinitypartners.data.entity.PlacePartnerCategory
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

@BindingAdapter("app:itemsRecentlyUpdate")
fun setItems(listView: RecyclerView, items: List<PlacePartnerCategory>) {
    (listView.adapter as RecentlyUpdateAdapter).submitList(items)
}