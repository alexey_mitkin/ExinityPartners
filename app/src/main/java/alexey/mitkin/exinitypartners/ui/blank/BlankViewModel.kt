package alexey.mitkin.exinitypartners.ui.blank

import alexey.mitkin.exinitypartners.Event
import alexey.mitkin.exinitypartners.data.sharedpref.SharedPreferenceContract
import alexey.mitkin.exinitypartners.ui.ScreenState
import alexey.mitkin.exinitypartners.ui.ScreenState.INTRO
import alexey.mitkin.exinitypartners.ui.ScreenState.LOADING
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import javax.inject.Inject

class BlankViewModel @Inject constructor(
    private val settings: SharedPreferenceContract
) : ViewModel() {

    private val _navigateToLoading = MutableLiveData<Event<Unit>>()
    val navigateToLoading: LiveData<Event<Unit>> = _navigateToLoading

    private val _navigateToOnboarding = MutableLiveData<Event<Unit>>()
    val navigateToOnboarding: LiveData<Event<Unit>> = _navigateToOnboarding

    private val _showError = MutableLiveData<Event<String>>()
    val showError:LiveData<Event<String>> = _showError


    fun startFragment() {
        if (settings.isFirstOpen) {
            navigation(INTRO)
        } else {
            navigation(LOADING)
        }
    }

    //TODO coverage more navigation case
    private fun navigation(state: ScreenState) {
        when (state) {
            INTRO -> _navigateToOnboarding.value = Event(Unit)
            LOADING -> _navigateToLoading.value = Event(Unit)
        }
    }
}