package alexey.mitkin.exinitypartners.ui.menu.list

import alexey.mitkin.exinitypartners.data.entity.Place
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView

@BindingAdapter("app:itemsPlace")
fun setItems(listView: RecyclerView, items: List<Place>) {
    (listView.adapter as PlaceByCategoryAdapter).submitList(items)
}