package alexey.mitkin.exinitypartners.ui.menu.list

import alexey.mitkin.exinitypartners.data.entity.Place
import alexey.mitkin.exinitypartners.databinding.ItemPlaceByCategoryBinding
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class PlaceByCategoryAdapter(private val viewModel: ListViewModel) :
    ListAdapter<Place, PlaceByCategoryAdapter.ViewHolder>(PlaceDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(viewModel, item)
        viewModel.setTextViewCategoryColorByPlace(item, holder.binding.textViewName)

        viewModel.getDistanceToPoint(item.latLng, holder.binding.textViewDistance)

    }

    class ViewHolder private constructor(val binding: ItemPlaceByCategoryBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(viewModel: ListViewModel, item: Place) {
            binding.viewmodel = viewModel
            binding.place = item
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemPlaceByCategoryBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}

class PlaceDiffCallback : DiffUtil.ItemCallback<Place>() {
    override fun areItemsTheSame(oldItem: Place, newItem: Place): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Place, newItem: Place): Boolean {
        return oldItem == newItem
    }
}