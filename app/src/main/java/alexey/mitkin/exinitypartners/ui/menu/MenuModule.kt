package alexey.mitkin.exinitypartners.ui.menu

import alexey.mitkin.exinitypartners.di.data.ViewModelKey
import alexey.mitkin.exinitypartners.ui.menu.details.DetailsFragment
import alexey.mitkin.exinitypartners.ui.menu.details.DetailsViewModel
import alexey.mitkin.exinitypartners.ui.menu.home.HomeFragment
import alexey.mitkin.exinitypartners.ui.menu.home.HomeViewModel
import alexey.mitkin.exinitypartners.ui.menu.list.ListFragment
import alexey.mitkin.exinitypartners.ui.menu.list.ListViewModel
import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class MenuModule {

    //=======================
    @ContributesAndroidInjector
    internal abstract fun contributeHomeFragment(): HomeFragment

    @Binds
    @IntoMap
    @ViewModelKey(HomeViewModel::class)
    abstract fun bindHomeViewModel(viewModel: HomeViewModel): ViewModel

    //=========================
    @ContributesAndroidInjector
    internal abstract fun contributeListFragment(): ListFragment

    @Binds
    @IntoMap
    @ViewModelKey(ListViewModel::class)
    abstract fun bindListViewModel(viewModel: ListViewModel): ViewModel

    //==========================
    @ContributesAndroidInjector
    internal abstract fun contributeBlankFragment(): DetailsFragment

    @Binds
    @IntoMap
    @ViewModelKey(DetailsViewModel::class)
    abstract fun bindDetailsViewModel(viewModel: DetailsViewModel): ViewModel
}