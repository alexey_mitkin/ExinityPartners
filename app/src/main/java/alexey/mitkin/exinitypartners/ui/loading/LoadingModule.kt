package alexey.mitkin.exinitypartners.ui.loading

import alexey.mitkin.exinitypartners.di.anotation.FragmentScoped
import alexey.mitkin.exinitypartners.di.data.ViewModelKey
import androidx.lifecycle.ViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class LoadingModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun contributeLoadingFragment(): LoadingFragment

    @Binds
    @IntoMap
    @ViewModelKey(LoadingViewModel::class)
    abstract fun bindLoadingViewModel(viewModel: LoadingViewModel): ViewModel
}