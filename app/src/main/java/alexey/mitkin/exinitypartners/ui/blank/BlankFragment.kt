package alexey.mitkin.exinitypartners.ui.blank

import alexey.mitkin.exinitypartners.EventObserver
import alexey.mitkin.exinitypartners.databinding.FragmentBlankBinding
import alexey.mitkin.exinitypartners.ui.base.BaseFragment
import alexey.mitkin.exinitypartners.ui.viewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController

class BlankFragment : BaseFragment() {

    lateinit var viewModel: BlankViewModel

    private lateinit var viewDataBinding: FragmentBlankBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = viewModelProvider(viewModelFactory)
        viewDataBinding = FragmentBlankBinding.inflate(inflater, container, false)
        viewDataBinding.viewmodel = viewModel
        viewDataBinding.lifecycleOwner = this
        return viewDataBinding.rootLayout
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setUpShowError()
        setUpNavigation()
        viewModel.startFragment()
    }

    private fun setUpShowError() {
        viewModel.showError.observe(viewLifecycleOwner, EventObserver {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        })
    }

    private fun setUpNavigation() {

        viewModel.navigateToOnboarding.observe(viewLifecycleOwner, EventObserver {
            val action = BlankFragmentDirections.actionBlankFragmentToOnboardingFragment()
            findNavController().navigate(action)
        })

        viewModel.navigateToLoading.observe(viewLifecycleOwner, EventObserver {
            val action = BlankFragmentDirections.actionBlankFragmentToLoadingFragment()
            findNavController().navigate(action)
        })
    }

}