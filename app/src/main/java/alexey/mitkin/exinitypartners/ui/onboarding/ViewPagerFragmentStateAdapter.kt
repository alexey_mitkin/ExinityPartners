package alexey.mitkin.exinitypartners.ui.onboarding

import alexey.mitkin.exinitypartners.R
import alexey.mitkin.exinitypartners.ui.onboarding.page.OnboardingPageFragment
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import javax.inject.Inject

class ViewPagerFragmentStateAdapter @Inject constructor(
    onboardingFragment: OnboardingFragment
) : FragmentStateAdapter(onboardingFragment.requireActivity()) {

    override fun getItemCount(): Int = 3

    @Inject
    lateinit var pageFirst: OnboardingPageFragment

    @Inject
    lateinit var pageSecond: OnboardingPageFragment

    @Inject
    lateinit var pageThird: OnboardingPageFragment

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> {
                val bundle = Bundle().apply {
                    putInt(OnboardingPageFragment.POSITION_KEY, position)
                    putInt(OnboardingPageFragment.TITLE_KEY, R.string.onboarding_text1)
                    putString(OnboardingPageFragment.JSON_NAME_KEY, "onboarding_discount.json")
                }
                pageFirst.apply { arguments = bundle }
            }
            1 -> {
                val bundle = Bundle().apply {
                    putInt(OnboardingPageFragment.POSITION_KEY, position)
                    putInt(OnboardingPageFragment.TITLE_KEY, R.string.onboarding_text2)
                    putString(OnboardingPageFragment.JSON_NAME_KEY, "onboarding_map.json")
                }
                pageSecond.apply { arguments = bundle }
            }
            else -> {
                val bundle = Bundle().apply {
                    putInt(OnboardingPageFragment.POSITION_KEY, position)
                    putInt(OnboardingPageFragment.TITLE_KEY, R.string.onboarding_text3)
                    putString(OnboardingPageFragment.JSON_NAME_KEY, "onboarding_offline.json")
                }
                pageThird.apply { arguments = bundle }
            }
        }
    }
}