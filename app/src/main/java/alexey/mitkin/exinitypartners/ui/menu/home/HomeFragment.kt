package alexey.mitkin.exinitypartners.ui.menu.home

import alexey.mitkin.exinitypartners.EventObserver
import alexey.mitkin.exinitypartners.databinding.FragmentCategoryHomeBinding
import alexey.mitkin.exinitypartners.ui.base.BaseFragment
import alexey.mitkin.exinitypartners.ui.getMapFragment
import alexey.mitkin.exinitypartners.ui.setNavigationBarColor
import alexey.mitkin.exinitypartners.ui.viewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_category_home.*

class HomeFragment : BaseFragment() {

    lateinit var viewModel: HomeViewModel

    private lateinit var viewDataBinding: FragmentCategoryHomeBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = viewModelProvider(viewModelFactory)
        viewDataBinding = FragmentCategoryHomeBinding.inflate(inflater, container, false)
        viewDataBinding.viewmodel = viewModel
        viewDataBinding.lifecycleOwner = this
        return viewDataBinding.rootLayout
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setNavigationBarColor(android.R.color.transparent)
        setupCategoriesGetting()
        setupShowPlacesByCategory()
        setupError()
        setupNavigation()
        setupAdapter()
        viewModel.startFragment()
    }

    private fun setupAdapter() {
        listRecentlyUpdate.adapter = RecentlyUpdateAdapter(viewModel)
    }

    private fun setupCategoriesGetting() {
        viewModel.categoriesItem.observe(viewLifecycleOwner, EventObserver {
            it.forEach(flexboxLayout::addView)
        })
    }

    private fun setupShowPlacesByCategory() {
        viewModel.showPlacesByCategory.observe(viewLifecycleOwner, EventObserver {
            getMapFragment().showPlacesByCategory(category = it)
        })

        viewModel.showAllPlaces.observe(viewLifecycleOwner, EventObserver{
            getMapFragment().showAllPlaces()
        })
    }

    private fun setupNavigation() {
        viewModel.navigateToPlacesListByCategory.observe(viewLifecycleOwner, EventObserver {
            val action = HomeFragmentDirections.actionHomeFragmentToListFragment(it)
            findNavController().navigate(action)
        })

        viewModel.navigateToAllPlacesList.observe(viewLifecycleOwner, EventObserver {
            val action = HomeFragmentDirections.actionHomeFragmentToListFragment(null)
            findNavController().navigate(action)
        })

        viewModel.navigateToPlaceDetails.observe(viewLifecycleOwner, EventObserver {
            getMapFragment().showPlaceOnMap(place = it)

            val action = HomeFragmentDirections.actionHomeFragmentToDetailsFragment(place = it)
            findNavController().navigate(action)
        })
    }

    private fun setupError() {
        viewModel.showError.observe(viewLifecycleOwner, EventObserver {
            showErrorDialog(it ?: "Error")
        })
    }
}