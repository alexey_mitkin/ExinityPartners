package alexey.mitkin.exinitypartners.ui

enum class ScreenState {
    INTRO, LOADING, MAP, OFFLINE
}