package alexey.mitkin.exinitypartners.ui.onboarding.page

import alexey.mitkin.exinitypartners.R
import alexey.mitkin.exinitypartners.data.sharedpref.SharedPreferenceStorage
import alexey.mitkin.exinitypartners.ui.onboarding.OnboardingFragmentDirections
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.airbnb.lottie.LottieDrawable.RESTART
import kotlinx.android.synthetic.main.fragment_onboarding_item.*
import javax.inject.Inject

class OnboardingPageFragment @Inject constructor() : Fragment() {

    @Inject
    lateinit var settings: SharedPreferenceStorage

    @StringRes
    private var title: Int = 0
    private var position: Int? = 0
    private var jsonName: String? = null

    companion object {
        const val POSITION_KEY = "position_args_key"
        const val JSON_NAME_KEY = "json_args_key"
        const val TITLE_KEY = "title_args_key"
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        jsonName = arguments?.getString(JSON_NAME_KEY)
        position = arguments?.getInt(POSITION_KEY)
        title = arguments?.getInt(TITLE_KEY)!!
        return inflater.inflate(R.layout.fragment_onboarding_item, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        buttonLetsStart.apply {
            visibility = if (position == 2) View.VISIBLE else View.GONE
            setOnClickListener {
                settings.isFirstOpen = false
                val action = OnboardingFragmentDirections.actionOnboardingFragmentToBlankFragment()
                findNavController().navigate(action)
            }
        }

        if (position == 1) {
            lottieView.repeatMode = RESTART
        }

        lottieView.setAnimation(jsonName)
        textViewInformation.text = getString(title)

    }
}