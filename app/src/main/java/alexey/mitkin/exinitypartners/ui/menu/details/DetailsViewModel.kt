package alexey.mitkin.exinitypartners.ui.menu.details

import alexey.mitkin.exinitypartners.Event
import alexey.mitkin.exinitypartners.data.entity.Place
import alexey.mitkin.exinitypartners.data.entity.PlacePartnerCategory
import alexey.mitkin.exinitypartners.data.local.ExinityPartnersDatabase
import alexey.mitkin.exinitypartners.util.ColorUtil
import android.graphics.Color
import androidx.lifecycle.*
import kotlinx.coroutines.launch
import javax.inject.Inject

class DetailsViewModel @Inject constructor(
    private val db: ExinityPartnersDatabase,
    private val colorUtil: ColorUtil
) : ViewModel() {


    val placePartnerCategory = MutableLiveData<PlacePartnerCategory>()

    val phone: LiveData<String> = Transformations.map(placePartnerCategory) {
        if (it.place.phone.isNotEmpty()) it.place.phone else it.partner.phone
    }

    val phoneAdditional: LiveData<String> = Transformations.map(placePartnerCategory) {
        if (it.place.phoneAdditional.isNotEmpty()) it.place.phoneAdditional else it.partner.phoneAdditional
    }

    val email: LiveData<String> = Transformations.map(placePartnerCategory) {
        if (it.place.email.isNotEmpty()) it.place.email else it.partner.email
    }

    val web: LiveData<String> = Transformations.map(placePartnerCategory) {
        if (it.place.web.isNotEmpty()) it.place.web else it.partner.web
    }

    private val _titleColor = MutableLiveData(Color.BLACK)
    val titleColor: LiveData<Int> = _titleColor

    private val _navigateBack = MutableLiveData<Event<Unit>>()
    val navigateBack: LiveData<Event<Unit>> = _navigateBack

    fun fragmentStart(place: Place) {
        viewModelScope.launch {
            placePartnerCategory.value =
                db.placePartnerCategoryDao().getPlacePartnerCategory(placeId = place.id)
        }
        viewModelScope.launch {
            _titleColor.value = colorUtil.getCategoryColorByPlace(place)
        }
    }

    fun onBackPress() {
        _navigateBack.value = Event(Unit)
    }

}