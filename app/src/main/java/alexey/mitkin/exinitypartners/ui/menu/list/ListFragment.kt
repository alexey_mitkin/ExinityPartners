package alexey.mitkin.exinitypartners.ui.menu.list

import alexey.mitkin.exinitypartners.EventObserver
import alexey.mitkin.exinitypartners.databinding.FragmentCategoryListBinding
import alexey.mitkin.exinitypartners.ui.base.BaseFragment
import alexey.mitkin.exinitypartners.ui.getMapFragment
import alexey.mitkin.exinitypartners.ui.viewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import kotlinx.android.synthetic.main.fragment_category_list.*

class ListFragment : BaseFragment() {

    lateinit var viewModel: ListViewModel

    private lateinit var viewDataBinding: FragmentCategoryListBinding

    private val args: ListFragmentArgs by navArgs()

    private lateinit var adapter: PlaceByCategoryAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = viewModelProvider(viewModelFactory)
        viewDataBinding = FragmentCategoryListBinding.inflate(inflater, container, false)
        viewDataBinding.viewmodel = viewModel
        viewDataBinding.lifecycleOwner = this
        return viewDataBinding.rootLayout
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupShowError()
        setupPlacesList()
        setupNavigation()
        viewModel.fragmentStart(args.category)
    }

    private fun setupPlacesList() {
        adapter = PlaceByCategoryAdapter(viewModel)
        placesList.adapter = adapter
    }

    private fun setupShowError() {
        viewModel.showError.observe(viewLifecycleOwner, EventObserver {
            showErrorDialog(it ?: "Same error")
        })
    }

    private fun setupNavigation() {
        viewModel.navigateToPlaceDetail.observe(viewLifecycleOwner, EventObserver {
            val action = ListFragmentDirections.actionListFragmentToDetailsFragment(it)
            findNavController().navigate(action)
            getMapFragment().showPlaceOnMap(place = it)
        })

        viewModel.navigateBack.observe(viewLifecycleOwner, EventObserver {
            findNavController().popBackStack()
        })
    }
}