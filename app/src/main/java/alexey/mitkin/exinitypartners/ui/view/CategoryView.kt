package alexey.mitkin.exinitypartners.ui.view

import alexey.mitkin.exinitypartners.R
import android.annotation.SuppressLint
import android.content.Context
import android.net.Uri
import android.util.AttributeSet
import android.view.View
import androidx.annotation.DrawableRes
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.view_category.view.*

@SuppressLint("ViewConstructor")
class CategoryView @JvmOverloads constructor(
    categoryName: String,
    imageUri: Uri? = null,
    @DrawableRes imageRes: Int? = null,
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    val root: View
        get() = rootLayout

    init {
        View.inflate(context, R.layout.view_category, this)
        textViewCategoryTitle.text = categoryName
        if (imageUri != null) {
            imageViewBackground.setImageURI(imageUri)
        } else if (imageRes != null) {
            imageViewBackground.setImageResource(imageRes)
        }
    }

}