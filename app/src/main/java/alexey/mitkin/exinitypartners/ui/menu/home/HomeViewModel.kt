package alexey.mitkin.exinitypartners.ui.menu.home

import alexey.mitkin.exinitypartners.AppRepository
import alexey.mitkin.exinitypartners.Event
import alexey.mitkin.exinitypartners.R
import alexey.mitkin.exinitypartners.data.Result
import alexey.mitkin.exinitypartners.data.entity.Category
import alexey.mitkin.exinitypartners.data.entity.Place
import alexey.mitkin.exinitypartners.data.entity.PlacePartnerCategory
import alexey.mitkin.exinitypartners.data.local.CacheImagesController
import alexey.mitkin.exinitypartners.ui.view.CategoryView
import alexey.mitkin.exinitypartners.util.ColorUtil
import alexey.mitkin.exinitypartners.util.TimeUtil
import android.content.Context
import android.widget.ImageView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeViewModel @Inject constructor(
    private val repository: AppRepository,
    private val cacheImagesController: CacheImagesController,
    private val colorUtil: ColorUtil,
    private val timeUtil: TimeUtil,
    private val context: Context
) : ViewModel() {

    private val _categoriesItems = MutableLiveData<Event<List<CategoryView>>>()
    val categoriesItem: LiveData<Event<List<CategoryView>>> = _categoriesItems

    private val _showError = MutableLiveData<Event<String?>>()
    val showError: LiveData<Event<String?>> = _showError

    private val _navigateToPlacesListByCategory = MutableLiveData<Event<Category>>()
    val navigateToPlacesListByCategory: LiveData<Event<Category>> = _navigateToPlacesListByCategory

    private val _navigateToAllPlacesList = MutableLiveData<Event<Unit>>()
    val navigateToAllPlacesList: LiveData<Event<Unit>> = _navigateToAllPlacesList

    private val _navigateToPlaceDetails = MutableLiveData<Event<Place>>()
    val navigateToPlaceDetails: LiveData<Event<Place>> = _navigateToPlaceDetails

    private val _showPlacesByCategory = MutableLiveData<Event<Category?>>()
    val showPlacesByCategory: LiveData<Event<Category?>> = _showPlacesByCategory

    private val _showAllPlaces = MutableLiveData<Event<Unit>>()
    val showAllPlaces: LiveData<Event<Unit>> = _showAllPlaces

    val recentlyUpdateItems = MutableLiveData<List<PlacePartnerCategory>>(emptyList())

    private var firstOpen = true

    private fun getAllCategories() {

        viewModelScope.launch {
            when (val result = repository.getAllCategories()) {
                is Result.Success -> {
                    val views = result.data
                        .map(this@HomeViewModel::generateCategoryViewByCategory)
                        .toMutableList()
                    views.add(generateAllCategoryView())
                    _categoriesItems.value = Event(views)
                }
                is Result.Error -> _showError.value = Event(result.exception.message)
            }
        }

        viewModelScope.launch {
            when (val result = repository.getRecentlyAdded()) {
                is Result.Success -> recentlyUpdateItems.value = result.data

                is Result.Error -> _showError.value = Event(result.exception.message)
            }
        }
    }

    private fun generateCategoryViewByCategory(category: Category): CategoryView {
        return CategoryView(
            categoryName = category.name,
            imageUri = cacheImagesController.getUriCategoryImage(category.id),
            context = context
        ).apply {
            root.setOnClickListener {
                _navigateToPlacesListByCategory.value = Event(category)
                _showPlacesByCategory.value = Event(category)
            }
        }
    }

    private fun generateAllCategoryView(): CategoryView =
        CategoryView(
            categoryName = context.getString(R.string.all),
            imageRes = R.drawable.ic_category_all,
            context = context
        ).apply {
            root.setOnClickListener {
                _navigateToAllPlacesList.value = Event(Unit)
            }
        }

    fun startFragment() {
        getAllCategories()
        if (firstOpen) {
            firstOpen = false
        } else {
            _showAllPlaces.value = Event(Unit)
        }
    }

    fun onClickRecentlyUpdateItem(place: Place) {
        _navigateToPlaceDetails.value = Event(place)
    }

    fun setCategoryImage(imageView: ImageView, category: Category) {
        val categoryImageUri = cacheImagesController.getUriCategoryImage(category.id)
        imageView.setImageURI(categoryImageUri)
    }

    fun getCategoryColor(category: Category): Int {
        return colorUtil.getCategoryColorByCategory(category)
    }

    fun getLastUpdate(place: Place): String {
        return timeUtil.getTimeAfterLastUpdate(place)
    }
}