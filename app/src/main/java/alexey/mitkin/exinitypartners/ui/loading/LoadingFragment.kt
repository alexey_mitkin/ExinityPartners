package alexey.mitkin.exinitypartners.ui.loading

import alexey.mitkin.exinitypartners.EventObserver
import alexey.mitkin.exinitypartners.R
import alexey.mitkin.exinitypartners.databinding.FragmentLoadingBinding
import alexey.mitkin.exinitypartners.ui.base.BaseFragment
import alexey.mitkin.exinitypartners.ui.setNavigationBarColor
import alexey.mitkin.exinitypartners.ui.viewModelProvider
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController

class LoadingFragment : BaseFragment() {

    lateinit var viewModel: LoadingViewModel

    lateinit var viewDataBinding: FragmentLoadingBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = viewModelProvider(viewModelFactory)
        viewDataBinding = FragmentLoadingBinding.inflate(layoutInflater, container, false)
        viewDataBinding.lifecycleOwner = this
        return viewDataBinding.rootLayout
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setNavigationBarColor(R.color.exinity_dark)
        setupNavigation()
        setupOfflineNotification()
        viewModel.onFragmentStart()
    }

    private fun setupOfflineNotification() {
        viewModel.showOfflineNotificationToast.observe(viewLifecycleOwner, EventObserver {
            Toast.makeText(requireContext(), R.string.offline, Toast.LENGTH_SHORT).show()
        })
    }

    private fun setupNavigation() {
        viewModel.navigateToMap.observe(viewLifecycleOwner, EventObserver {
            val action = LoadingFragmentDirections.actionLoadingFragmentToMapFragment()
            findNavController().navigate(action)
        })

        viewModel.navigateToOffline.observe(viewLifecycleOwner, EventObserver {
            val action = LoadingFragmentDirections.actionLoadingFragmentToOfflineFragment()
            findNavController().navigate(action)
        })
    }
}