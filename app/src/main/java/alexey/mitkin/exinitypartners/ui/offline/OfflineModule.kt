package alexey.mitkin.exinitypartners.ui.offline

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class OfflineModule {

    @ContributesAndroidInjector
    abstract fun contributesOfflineFragment(): OfflineFragment
}