package alexey.mitkin.exinitypartners.ui.map

import alexey.mitkin.exinitypartners.EventObserver
import alexey.mitkin.exinitypartners.R
import alexey.mitkin.exinitypartners.data.entity.Category
import alexey.mitkin.exinitypartners.data.entity.Place
import alexey.mitkin.exinitypartners.databinding.FragmentMapBinding
import alexey.mitkin.exinitypartners.ui.base.BaseMapFragment
import alexey.mitkin.exinitypartners.ui.menu.details.DetailsFragmentArgs
import alexey.mitkin.exinitypartners.ui.viewModelProvider
import alexey.mitkin.exinitypartners.util.LocationUtil
import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.findNavController
import androidx.navigation.navOptions
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.*
import kotlinx.android.synthetic.main.fragment_map.*
import kotlinx.android.synthetic.main.menu_bottom.*
import kotlinx.coroutines.launch
import javax.inject.Inject


class MapFragment : BaseMapFragment(), GoogleMap.OnMarkerClickListener {

    lateinit var viewModel: MapViewModel

    private lateinit var viewDataBinding: FragmentMapBinding

    lateinit var behavior: BottomSheetBehavior<LinearLayout>

    private var locationPermissionGranted = false

    @Inject
    lateinit var locationUtil: LocationUtil

    private val defaultLocation = LatLng(34.666956, 33.040774)

    private val DEFAULT_ZOOM = 13f

    private val HOME_ZOOM = 18f

    private val markersMap = HashMap<String, Marker>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = viewModelProvider(viewModelFactory)
        viewDataBinding = FragmentMapBinding.inflate(inflater, container, false)
        viewDataBinding.viewmodel = viewModel
        //google maps
        mapView = viewDataBinding.mapView
        mapView?.onCreate(savedInstanceState)
        mapView?.getMapAsync(this)

        getLocationPermission()
        return viewDataBinding.rootLayout
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupBottomMenuBehavior()
        setupBottomMenuTopPadding()

        buttonExpandMenu.setOnClickListener {
            behavior.state = STATE_HALF_EXPANDED
        }

        buttonLocation.setOnClickListener {
            getLocationPermission(fromButtonLocation = true)
        }

        setupShowError()

        activity?.window?.statusBarColor =
            ContextCompat.getColor(requireContext(), R.color.black_30)
    }

    private fun setupBottomMenuTopPadding() {
        val resourceId = resources.getIdentifier("status_bar_height", "dimen", "android")
        val statusBarHeight = resources.getDimensionPixelSize(resourceId)

        logD("Status bar size $statusBarHeight px")
        bottom_sheet_layout.setPadding(0, statusBarHeight, 0, 0)

    }

    private fun setupBottomMenuBehavior() {
        behavior = from(bottom_sheet_layout)
        behavior.state = STATE_HALF_EXPANDED

        behavior.addBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {}

            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    STATE_COLLAPSED -> {
                        logD("State collapsed")
                        buttonExpandMenu.visibility = VISIBLE
                    }
                    STATE_EXPANDED -> logD("State expanded")
                    STATE_HALF_EXPANDED -> logD("State half expanded")
                    STATE_DRAGGING -> logD("State dragging")
                    STATE_HIDDEN -> logD("State hidden")
                    STATE_SETTLING -> {
                        logD("State settling")
                        buttonExpandMenu.visibility = GONE
                    }
                }
            }
        })
    }

    override fun onMapReady(googleMap: GoogleMap) {
        super.onMapReady(googleMap)
        viewModel.onMapReady()
        googleMap.setOnMarkerClickListener(this)
        updateLocationUI()
        setupPlacesOnMap()
        gMap.moveCamera(
            CameraUpdateFactory.newLatLngZoom(
                defaultLocation,
                DEFAULT_ZOOM
            )
        )
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        if (marker.tag == null) return false
        behavior.state = STATE_HALF_EXPANDED
        val bottomNavController = activity?.findNavController(R.id.nav_host_fragment_menu)
        val args = DetailsFragmentArgs(marker.tag as Place)
        val navOption = navOptions {
            anim {
                enter = R.anim.screen_slide_in_right
                exit = R.anim.screen_slide_out_left
                popEnter = R.anim.screen_slide_in_left
                popExit = R.anim.screen_slide_out_right
            }
        }
        bottomNavController?.navigate(R.id.detailsFragment, args.toBundle(), navOption)
        return false
    }

    private fun setupPlacesOnMap() {
        viewModel.markerOptionsItems.observe(
            viewLifecycleOwner,
            EventObserver { placesMarketsList ->
                gMap.clear()
                markersMap.clear()
                placesMarketsList.forEach {
                    markersMap[it.first.id] = gMap.addMarker(it.second).apply {
                        tag = it.first
                    }
                }
            })
    }

    private fun setupShowError() {
        viewModel.showError.observe(viewLifecycleOwner, EventObserver {
            showErrorDialog(it)
        })
    }

    fun showPlacesByCategory(category: Category?) {
        viewModel.showPlacesByCategory(category)
    }

    fun showAllPlaces() {
        viewModel.showPlacesByCategory(null)
    }

    fun showPlaceOnMap(place: Place) {
        markersMap[place.id]?.let {
            it.showInfoWindow()
            behavior.state = STATE_HALF_EXPANDED
            moveCameraToLatLng(it.position, HOME_ZOOM)
        }
    }

    private fun updateLocationUI() {
        try {
            if (locationPermissionGranted) {
                gMap.isMyLocationEnabled = true
                gMap.uiSettings.isMyLocationButtonEnabled = false
            }

            getDeviceLocation()

        } catch (se: SecurityException) {
            se.printStackTrace()
            showErrorDialog(se.localizedMessage ?: "Security location error")
        }
    }

    private fun getDeviceLocation() {
        if (locationPermissionGranted) {

            lifecycleScope.launch {
                val latLng = locationUtil.getLastLocation()
                if (latLng != null) {
                    moveCameraToLatLng(latLng, HOME_ZOOM)
                } else {
                    logD("Problem when getting last location")
                    moveCameraToLatLng(defaultLocation, DEFAULT_ZOOM)

                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        locationPermissionGranted = false
        when (requestCode) {
            PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty()
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    locationPermissionGranted = true
                    updateLocationUI()
                }
            }
        }

    }

    private fun getLocationPermission(fromButtonLocation: Boolean = false) {
        /*
     * Request location permission, so that we can get the location of the
     * device. The result of the permission request is handled by a callback,
     * onRequestPermissionsResult.
     */
        if (ContextCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            logD("Permission already granted")
            locationPermissionGranted = true
            if (fromButtonLocation) {
                updateLocationUI()
            }
        } else {
            requestPermissions(
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
            )
        }
    }

    private fun moveCameraToLatLng(latLng: LatLng?, zoom: Float) {
        gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom))
    }

    companion object {
        const val PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1563
    }

}