package alexey.mitkin.exinitypartners.ui.base

import alexey.mitkin.exinitypartners.BuildConfig
import alexey.mitkin.exinitypartners.di.data.ViewModelFactory
import android.util.Log
import androidx.appcompat.app.AlertDialog
import dagger.android.support.DaggerFragment
import javax.inject.Inject

open class BaseFragment : DaggerFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    val TAG = this.javaClass.simpleName

    fun logD(arg: Any = "") {
        if (BuildConfig.DEBUG) Log.d(javaClass.simpleName, arg.toString())
    }

    fun showErrorDialog(error: String) {
        AlertDialog.Builder(requireActivity())
            .setTitle("Error")
            .setPositiveButton("ok") { _, _ -> }
            .setMessage(error)
            .show()
    }
}