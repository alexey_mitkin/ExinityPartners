package alexey.mitkin.exinitypartners.ui.offline

import alexey.mitkin.exinitypartners.databinding.FragmentOfflineBinding
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_offline.*

class OfflineFragment : DaggerFragment() {

    lateinit var viewDataBinding: FragmentOfflineBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewDataBinding = FragmentOfflineBinding.inflate(inflater, container, false)
        return viewDataBinding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        buttonTryAgain.setOnClickListener {
            val action = OfflineFragmentDirections.actionOfflineFragmentToBlankFragment()
            findNavController().navigate(action)
        }
    }
}