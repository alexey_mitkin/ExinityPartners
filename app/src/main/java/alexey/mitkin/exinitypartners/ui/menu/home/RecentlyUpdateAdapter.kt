package alexey.mitkin.exinitypartners.ui.menu.home

import alexey.mitkin.exinitypartners.data.entity.PlacePartnerCategory
import alexey.mitkin.exinitypartners.databinding.ItemRecentlyUpdateBinding
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView

class RecentlyUpdateAdapter(private val viewModel: HomeViewModel) :
    ListAdapter<PlacePartnerCategory, RecentlyUpdateAdapter.ViewHolder>(PlaceDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = getItem(position)
        holder.bind(viewModel, item)
        viewModel.setCategoryImage(holder.binding.imageViewCategory, item.category)
    }

    class ViewHolder private constructor(val binding: ItemRecentlyUpdateBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(viewModel: HomeViewModel, item: PlacePartnerCategory) {
            binding.viewmodel = viewModel
            binding.placePartnerCategory = item
            binding.executePendingBindings()
        }

        companion object {
            fun from(parent: ViewGroup): ViewHolder {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemRecentlyUpdateBinding.inflate(layoutInflater, parent, false)

                return ViewHolder(binding)
            }
        }
    }
}

class PlaceDiffCallback : DiffUtil.ItemCallback<PlacePartnerCategory>() {
    override fun areItemsTheSame(
        oldItem: PlacePartnerCategory,
        newItem: PlacePartnerCategory
    ): Boolean {
        return oldItem.place.id == newItem.place.id
    }

    override fun areContentsTheSame(
        oldItem: PlacePartnerCategory,
        newItem: PlacePartnerCategory
    ): Boolean {
        return oldItem == newItem
    }
}