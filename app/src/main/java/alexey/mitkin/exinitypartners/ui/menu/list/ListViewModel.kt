package alexey.mitkin.exinitypartners.ui.menu.list

import alexey.mitkin.exinitypartners.AppRepository
import alexey.mitkin.exinitypartners.Event
import alexey.mitkin.exinitypartners.R
import alexey.mitkin.exinitypartners.data.Result
import alexey.mitkin.exinitypartners.data.entity.Category
import alexey.mitkin.exinitypartners.data.entity.Place
import alexey.mitkin.exinitypartners.util.ColorUtil
import alexey.mitkin.exinitypartners.util.DistanceUtil
import alexey.mitkin.exinitypartners.util.LocationUtil
import android.content.Context
import android.graphics.Color
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.TextView
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.SphericalUtil
import kotlinx.coroutines.launch
import javax.inject.Inject

class ListViewModel @Inject constructor(
    private val repository: AppRepository,
    private val context: Context,
    private val colorUtil: ColorUtil,
    private val locationUtil: LocationUtil,
    private val distanceUtil: DistanceUtil
) : ViewModel() {

    private val _placesItems = MutableLiveData<List<Place>>().apply { value = emptyList() }
    val placesItems: LiveData<List<Place>> = _placesItems

    private val _showError = MutableLiveData<Event<String?>>()
    val showError: LiveData<Event<String?>> = _showError

    private val _navigateToPlaceDetail = MutableLiveData<Event<Place>>()
    val navigateToPlaceDetail: LiveData<Event<Place>> = _navigateToPlaceDetail

    val name = MutableLiveData<String>()

    private val _titleColor = MutableLiveData(Color.BLACK)
    val titleColor: LiveData<Int> = _titleColor

    private val _navigateBack = MutableLiveData<Event<Unit>>()
    val navigateBack: LiveData<Event<Unit>> = _navigateBack

    fun fragmentStart(category: Category?) {
        this.name.value = category?.name ?: context.getString(R.string.all)
        viewModelScope.launch {

            val result = if (category != null) {
                _titleColor.value = colorUtil.getCategoryColorByCategory(category)
                repository.getPlacesByCategory(category.id)
            } else {
                repository.getAllPlaces()
            }

            when (result) {
                is Result.Success -> _placesItems.value = result.data
                is Result.Error -> _showError.value = Event(result.exception.localizedMessage)
            }
        }
    }

    fun onClickPlace(place: Place) {
        _navigateToPlaceDetail.value = Event(place)
    }

    fun onBackPress() {
        _navigateBack.value = Event(Unit)
    }

    fun setTextViewCategoryColorByPlace(place: Place, textView: TextView) {
        viewModelScope.launch {
            textView.setTextColor((colorUtil.getCategoryColorByPlace(place)))
        }
    }

    fun getDistanceToPoint(latLng: LatLng?, textViewDistance: TextView) {
        textViewDistance.visibility = GONE
        if (latLng == null) return

        viewModelScope.launch {
            locationUtil.getLastLocation()?.let {
                val distance = SphericalUtil.computeDistanceBetween(latLng, it)
                textViewDistance.apply {
                    visibility = VISIBLE
                    text = distanceUtil.convertMetersToString(distance)
                }
            }
        }
    }

}