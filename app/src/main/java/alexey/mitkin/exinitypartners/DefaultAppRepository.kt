package alexey.mitkin.exinitypartners

import alexey.mitkin.exinitypartners.data.CategoryDataSource
import alexey.mitkin.exinitypartners.data.PartnerDataSource
import alexey.mitkin.exinitypartners.data.PlaceDataSource
import alexey.mitkin.exinitypartners.data.Result
import alexey.mitkin.exinitypartners.data.di.CategoryModule.CategoryRemoteDataSource
import alexey.mitkin.exinitypartners.data.di.PartnerModule.PartnerRemoteDataSource
import alexey.mitkin.exinitypartners.data.di.PlaceModule.PlaceLocalDataSource
import alexey.mitkin.exinitypartners.data.di.PlaceModule.PlaceRemoteDataSource
import alexey.mitkin.exinitypartners.data.entity.Category
import alexey.mitkin.exinitypartners.data.entity.Place
import alexey.mitkin.exinitypartners.data.entity.PlacePartnerCategory
import alexey.mitkin.exinitypartners.data.exeption.FetchDataException
import alexey.mitkin.exinitypartners.data.local.CacheImagesController
import alexey.mitkin.exinitypartners.data.local.ExinityPartnersDatabase
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.withContext
import javax.inject.Inject

class DefaultAppRepository @Inject constructor(
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
    @PlaceRemoteDataSource private val placeRemoteDataSource: PlaceDataSource,
    @PlaceLocalDataSource private val placeLocalDataSource: PlaceDataSource,
    @PartnerRemoteDataSource private val partnerRemoteDataSource: PartnerDataSource,
    @CategoryRemoteDataSource private val categoryRemoteDataSource: CategoryDataSource,
    private val categoryImageRemoteDS: ImagesDataSource,
    private val cacheImagesController: CacheImagesController,
    private val db: ExinityPartnersDatabase
) : AppRepository {

    override suspend fun fetch(): Result<Unit> = withContext(ioDispatcher) {
        return@withContext try {

            val placeResult = async {
                when (val places = placeRemoteDataSource.getPlaces()) {
                    is Result.Success -> {
                        db.placeDao().deleteAll()
                        places.data.forEach { db.placeDao().insertPlace(it) }
                    }
                    else -> Result.Error((places as Result.Error).exception)
                }
            }

            val partnerResult = async {
                when (val partners = partnerRemoteDataSource.getPartners()) {
                    is Result.Success -> {
                        db.partnerDao().deleteAll()
                        partners.data.forEach { db.partnerDao().insertPartner(it) }
                    }
                    else -> Result.Error((partners as Result.Error).exception)
                }
            }

            val categoryResult = async {
                when (val category = categoryRemoteDataSource.getCategories()) {
                    is Result.Success -> {
                        db.categoryDao().deleteAll()
                        category.data.forEach {
                            db.categoryDao().insertCategory(it)

                            if (cacheImagesController.isNeedUpdateCategoryImage(it)) {
                                val categoryImage = categoryImageRemoteDS
                                    .getImageBytesById(it.id)

                                cacheImagesController.saveCategoryImage(it, categoryImage)
                            }

                            if (cacheImagesController.isNeedUpdateCategoryMarkerImage(it)) {
                                val categoryMarkerImage = categoryImageRemoteDS
                                    .getImageBytesById("${it.id}_marker")

                                cacheImagesController.saveCategoryMarkerImage(
                                    category = it,
                                    content = categoryMarkerImage
                                )
                            }
                        }

                    }
                    else -> Result.Error((category as Result.Error).exception)
                }
            }

            if (placeResult.await() !is Result.Error
                && categoryResult.await() !is Result.Error
                && partnerResult.await() !is Result.Error
            ) {
                Result.Success(Unit)
            } else {
                Result.Error(FetchDataException())
            }
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    override suspend fun getRecentlyAdded(): Result<List<PlacePartnerCategory>> =
        withContext(ioDispatcher){
            return@withContext try {
                Result.Success(db.placePartnerCategoryDao().getRecentlyAdded())
            }catch (e:Exception){
                Result.Error(e)
            }
        }

    override suspend fun getAllPlaces(): Result<List<Place>> =
        withContext(ioDispatcher) {
            return@withContext try {
                placeLocalDataSource.getPlaces()
            } catch (e: Exception) {
                Result.Error(e)
            }
        }

    override suspend fun getAllCategories(): Result<List<Category>> = withContext(ioDispatcher) {
        return@withContext try {
            Result.Success(db.categoryDao().getCategories())
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    override suspend fun getAllPlacesPartnersAndCategory(forceUpdate: Boolean): Result<List<PlacePartnerCategory>> {
        TODO("Not yet impl")
    }

    override suspend fun getPlacesByCategory(categoryId: String): Result<List<Place>> =
        withContext(ioDispatcher) {
            return@withContext try {
                Result.Success(db.placeDao().getAllPlacesByCategory(categoryId))
            } catch (e: Exception) {
                Result.Error(e)
            }
        }
}