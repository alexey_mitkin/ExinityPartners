package alexey.mitkin.exinitypartners

import alexey.mitkin.exinitypartners.data.Result
import alexey.mitkin.exinitypartners.data.entity.Category
import alexey.mitkin.exinitypartners.data.entity.Place
import alexey.mitkin.exinitypartners.data.entity.PlacePartnerCategory

interface AppRepository {

    suspend fun fetch(): Result<Unit>

    suspend fun getRecentlyAdded(): Result<List<PlacePartnerCategory>>

    suspend fun getAllPlaces(): Result<List<Place>>

    suspend fun getAllPlacesPartnersAndCategory(forceUpdate: Boolean = false): Result<List<PlacePartnerCategory>>

    suspend fun getPlacesByCategory(categoryId: String): Result<List<Place>>

    suspend fun getAllCategories(): Result<List<Category>>
}