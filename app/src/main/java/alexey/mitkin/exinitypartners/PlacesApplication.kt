package alexey.mitkin.exinitypartners

import alexey.mitkin.exinitypartners.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class PlacesApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.factory().create(applicationContext)
    }

}