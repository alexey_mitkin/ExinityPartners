package alexey.mitkin.exinitypartners.util

import alexey.mitkin.exinitypartners.data.entity.Category
import alexey.mitkin.exinitypartners.data.entity.Place
import alexey.mitkin.exinitypartners.data.local.ExinityPartnersDatabase
import alexey.mitkin.exinitypartners.data.sharedpref.SharedPreferenceContract
import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.Color
import androidx.palette.graphics.Palette
import java.io.File
import javax.inject.Inject

class ColorUtil @Inject constructor(
    private val settings: SharedPreferenceContract,
    private val db: ExinityPartnersDatabase,
    private val context: Context
) {

    suspend fun getCategoryColorByPlace(place: Place): Int {
        val categoryId = db.categoryDao().getCategoryByPlace(placeId = place.id).id
        return settings.getCategoryColor(categoryId)
    }

    fun getCategoryColorByCategory(category: Category): Int {
        return settings.getCategoryColor(category.id)
    }

    fun saveDominantColorForCategory(categoryId: String) {
        //get saved image bitmap
        val bitmap = BitmapFactory.decodeFile(
            File(context.cacheDir, "$categoryId.png").absolutePath
        )
        Palette.Builder(bitmap).generate {
            it?.let { palette ->
                val dominantColor = palette.getDominantColor(Color.BLACK)
                settings.saveCategoryColor(dominantColor, categoryId)
            }
        }
    }
}