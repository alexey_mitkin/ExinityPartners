package alexey.mitkin.exinitypartners.util

import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.tasks.await
import javax.inject.Inject

class LocationUtil @Inject constructor(
    private val fusedLocationProviderClient: FusedLocationProviderClient
) {

    suspend fun getLastLocation(): LatLng? {

        return try {
            val location = fusedLocationProviderClient.lastLocation.await()
            LatLng(location.latitude, location.longitude)
        } catch (se: SecurityException) {
            null
        }
    }
}