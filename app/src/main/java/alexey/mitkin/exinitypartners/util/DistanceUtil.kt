package alexey.mitkin.exinitypartners.util

import alexey.mitkin.exinitypartners.R
import android.content.Context
import java.math.RoundingMode
import javax.inject.Inject

class DistanceUtil @Inject constructor(
    private val context: Context
) {

    fun convertMetersToString(meters: Double): String {
        return if (meters > 1000.0) {
            val km = meters / 1000
            "${km.toBigDecimal().setScale(2, RoundingMode.UP)} ${context.getString(R.string.km)}"
        } else {
            "${meters.toBigDecimal().setScale(2, RoundingMode.UP)} ${context.getString(R.string.m)}"
        }
    }
}