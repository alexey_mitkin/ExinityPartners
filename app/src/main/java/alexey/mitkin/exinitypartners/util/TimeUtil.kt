package alexey.mitkin.exinitypartners.util

import alexey.mitkin.exinitypartners.R
import alexey.mitkin.exinitypartners.data.entity.Place
import android.content.Context
import javax.inject.Inject

class TimeUtil @Inject constructor(
    val context: Context
) {

    fun getTimeAfterLastUpdate(place: Place): String {
        val mills = System.currentTimeMillis() - place.createTs
        val days = getDays(mills)
        val minutes = getMinutes(mills)
        val hours = getHours(mills)
        return when {
            days != 0L -> "$days ${context.getString(R.string.days)} ${context.getString(R.string.ago)}"
            hours != 0L -> "$hours ${context.getString(R.string.hours)} ${context.getString(R.string.ago)}"
            else -> "$minutes ${context.getString(R.string.minutes)} ${context.getString(R.string.ago)}"
        }
    }

    private fun getDays(mills: Long): Long {
        return mills / (1000 * 60 * 60 * 24)
    }

    private fun getHours(mills: Long): Long {
        return mills / (1000 * 60 * 60)
    }

    private fun getMinutes(mills: Long): Long {
        return mills / (1000 * 60)
    }

}