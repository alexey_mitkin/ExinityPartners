package alexey.mitkin.exinitypartners.data.remote

import alexey.mitkin.exinitypartners.ImagesDataSource
import alexey.mitkin.exinitypartners.TIME_OUT_SECOND
import alexey.mitkin.proto.DownloadImageRequest
import alexey.mitkin.proto.UploadImageControlGrpc
import com.google.protobuf.ByteString
import io.grpc.ManagedChannel
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ImagesRemoteDataSource @Inject constructor(
    private val channel: ManagedChannel
) : ImagesDataSource {
    override suspend fun getImageBytesById(categoryId: String): ByteString {
        val request = DownloadImageRequest.newBuilder().setId(categoryId).build()
        val stub = UploadImageControlGrpc.newBlockingStub(channel)
        val reply = stub.withDeadlineAfter(TIME_OUT_SECOND, TimeUnit.SECONDS).downloadImage(request)
        return reply.data
    }
}