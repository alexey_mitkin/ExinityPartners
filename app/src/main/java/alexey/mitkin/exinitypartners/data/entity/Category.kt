package alexey.mitkin.exinitypartners.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class Category constructor(
    @PrimaryKey @ColumnInfo(name = "category_id") var id: String,
    @ColumnInfo(name = "category_createTs") var createTs: Long,
    @ColumnInfo(name = "category_createdBy") var createdBy: String,
    @ColumnInfo(name = "category_updateTs") var updateTs: Long,
    @ColumnInfo(name = "category_updatedBy") var updatedBy: String,
    @ColumnInfo(name = "category_name") var name: String,
    @ColumnInfo(name = "category_description") var description: String
) : Serializable