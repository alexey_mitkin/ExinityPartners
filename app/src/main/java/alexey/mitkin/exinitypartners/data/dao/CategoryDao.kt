package alexey.mitkin.exinitypartners.data.dao

import alexey.mitkin.exinitypartners.data.entity.Category
import androidx.room.*

@Dao
interface CategoryDao {

    @Query("SELECT * FROM Category")
    suspend fun getCategories(): List<Category>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCategory(category: Category)

    @Update
    suspend fun updateCategory(category: Category): Int

    @Query("DELETE FROM Category")
    fun deleteAll()

    @Transaction
    @Query("SELECT Category.* FROM Place INNER JOIN Partner ON Place.partnerId = Partner.partner_id INNER JOIN Category ON Partner.categoryId = Category.category_id WHERE Place.place_id = :placeId")
    suspend fun getCategoryByPlace(placeId: String): Category
}