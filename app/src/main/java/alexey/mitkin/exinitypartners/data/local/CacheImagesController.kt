package alexey.mitkin.exinitypartners.data.local

import alexey.mitkin.exinitypartners.data.entity.Category
import alexey.mitkin.exinitypartners.data.sharedpref.SharedPreferenceContract
import alexey.mitkin.exinitypartners.util.ColorUtil
import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import com.google.protobuf.ByteString
import java.io.File
import java.io.FileOutputStream
import javax.inject.Inject

class CacheImagesController @Inject constructor(
    private val context: Context,
    private val settings: SharedPreferenceContract,
    private val colorUtil: ColorUtil
) {

    private val TAG = this.javaClass.simpleName

    fun saveCategoryImage(category: Category, content: ByteString) {
        saveImage("${category.id}.png", content)
        settings.saveLastUpdateCategoryImage(category)

        colorUtil.saveDominantColorForCategory(category.id)
    }

    fun isNeedUpdateCategoryImage(category: Category): Boolean {
        val file = File(context.cacheDir, "${category.id}.png")
        if (!file.exists()) return true
        return !settings.isTheActualVersionCategoryImageSave(category)
    }

    fun isNeedUpdateCategoryMarkerImage(category: Category): Boolean {
        val file = File(context.cacheDir, "${category.id}_marker.png")
        if (!file.exists()) return true
        return !settings.isTheActualVersionCategoryImageSave(category)
    }

    fun saveCategoryMarkerImage(category: Category, content: ByteString) {
        saveImage("${category.id}_marker.png", content)
        settings.saveLastUpdateCategoryImage(category)
    }

    fun getCategoryMarker(id: String): Bitmap {

        val categoryMarkerBitmap = BitmapFactory
            .decodeFile(File(context.cacheDir, "${id}_marker.png").absolutePath)

        return Bitmap.createScaledBitmap(categoryMarkerBitmap, 76, 110, false)
    }

    fun getUriCategoryImage(id: String): Uri =
        Uri.fromFile(File(context.cacheDir, "$id.png"))

    private fun saveImage(fileName: String, content: ByteString) {
        val file = File(context.cacheDir, fileName)
        val outputStream = FileOutputStream(file)
        content.writeTo(outputStream)
        outputStream.close()
    }
}