package alexey.mitkin.exinitypartners.data.remote

import alexey.mitkin.exinitypartners.TIME_OUT_SECOND
import alexey.mitkin.exinitypartners.data.CategoryDataSource
import alexey.mitkin.exinitypartners.data.Result
import alexey.mitkin.exinitypartners.data.entity.Category
import alexey.mitkin.proto.CategoryControlGrpc
import alexey.mitkin.proto.CategoryRequest
import io.grpc.ManagedChannel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CategoryRemoteDataSource @Inject constructor(
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
    private val channel: ManagedChannel
) : CategoryDataSource {

    override suspend fun getCategories(): Result<List<Category>> = withContext(ioDispatcher) {
        return@withContext try {
            val stub = CategoryControlGrpc.newBlockingStub(channel)
            val request = CategoryRequest.getDefaultInstance()
            val reply = stub.withDeadlineAfter(TIME_OUT_SECOND, TimeUnit.SECONDS).getCategories(request)
            val newList = reply.categoriesList.map {
                Category(
                    it.id,
                    it.createTs,
                    it.createdBy,
                    it.updateTs,
                    it.updatedBy,
                    it.name,
                    it.description
                )
            }
            Result.Success(newList)
        } catch (e: Exception) {
            Result.Error(e)
        }
    }
}
