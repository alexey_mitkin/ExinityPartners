package alexey.mitkin.exinitypartners.data.entity

import androidx.room.Embedded

data class PlacePartnerCategory(
    @Embedded val place: Place,
    @Embedded val partner: Partner,
    @Embedded val category: Category
)