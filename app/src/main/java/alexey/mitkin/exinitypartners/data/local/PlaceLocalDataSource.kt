package alexey.mitkin.exinitypartners.data.local

import alexey.mitkin.exinitypartners.data.PlaceDataSource
import alexey.mitkin.exinitypartners.data.Result
import alexey.mitkin.exinitypartners.data.dao.PlaceDao
import alexey.mitkin.exinitypartners.data.entity.Place
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PlaceLocalDataSource @Inject constructor(
    private val placeDao: PlaceDao,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : PlaceDataSource {

    override suspend fun getPlaces(): Result<List<Place>> = withContext(ioDispatcher) {
        return@withContext try {
            Result.Success(placeDao.getPlaces())
        } catch (e: Exception) {
            Result.Error(e)
        }
    }
}