package alexey.mitkin.exinitypartners.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.android.gms.maps.model.LatLng
import java.io.Serializable

@Entity
data class Place constructor(
    @PrimaryKey @ColumnInfo(name = "place_id") var id: String,
    @ColumnInfo(name = "place_createTs") var createTs: Long,
    @ColumnInfo(name = "place_createdBy") var createdBy: String,
    @ColumnInfo(name = "place_updateTs") var updateTs: Long,
    @ColumnInfo(name = "place_updatedBy") var updatedBy: String,
    @ColumnInfo(name = "place_name") var name: String,
    @ColumnInfo(name = "place_address") var address: String,
    @ColumnInfo(name = "partnerId") var partnerId: String,
    @ColumnInfo(name = "place_discountRate") var discountRate: String,
    @ColumnInfo(name = "place_coordinates") var coordinates: String,
    @ColumnInfo(name = "place_monday") var monday: String,
    @ColumnInfo(name = "place_tuesday") var tuesday: String,
    @ColumnInfo(name = "place_wednesday") var wednesday: String,
    @ColumnInfo(name = "place_thursday") var thursday: String,
    @ColumnInfo(name = "place_friday") var friday: String,
    @ColumnInfo(name = "place_saturday") var saturday: String,
    @ColumnInfo(name = "place_sunday") var sunday: String,
    @ColumnInfo(name = "place_card_required") var isCardRequired: Boolean,
    @ColumnInfo(name = "place_phone") var phone: String,
    @ColumnInfo(name = "place_phone_additional") var phoneAdditional: String,
    @ColumnInfo(name = "place_email") var email: String,
    @ColumnInfo(name = "place_web") var web: String
) : Serializable {

    val latLng: LatLng?
        get() {
            if (coordinates.isEmpty()) return null
            val latLngString = this.coordinates.split(",")
            return LatLng(latLngString[0].toDouble(), latLngString[1].toDouble())
        }
}