package alexey.mitkin.exinitypartners.data

import alexey.mitkin.exinitypartners.data.entity.Category

interface CategoryDataSource {

    suspend fun getCategories(): Result<List<Category>>
}