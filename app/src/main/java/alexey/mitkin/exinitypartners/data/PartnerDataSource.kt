package alexey.mitkin.exinitypartners.data

import alexey.mitkin.exinitypartners.data.entity.Partner

interface PartnerDataSource {
    suspend fun getPartners(): Result<List<Partner>>
}