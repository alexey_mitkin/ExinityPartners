package alexey.mitkin.exinitypartners.data.dao

import alexey.mitkin.exinitypartners.data.entity.Partner
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface PartnerDao {
    @Query("SELECT * FROM Partner")
    suspend fun getPartners(): List<Partner>

    @Query("SELECT * FROM Partner WHERE partner_id = :id")
    suspend fun getPartner(id: String): Partner

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPartner(partner: Partner)

    @Query("DELETE FROM Partner")
    suspend fun deleteAll()
}