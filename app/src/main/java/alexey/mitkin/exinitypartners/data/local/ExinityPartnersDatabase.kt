package alexey.mitkin.exinitypartners.data.local

import alexey.mitkin.exinitypartners.data.dao.CategoryDao
import alexey.mitkin.exinitypartners.data.dao.PartnerDao
import alexey.mitkin.exinitypartners.data.dao.PlaceDao
import alexey.mitkin.exinitypartners.data.dao.PlacePartnerCategoryDao
import alexey.mitkin.exinitypartners.data.entity.Category
import alexey.mitkin.exinitypartners.data.entity.Partner
import alexey.mitkin.exinitypartners.data.entity.Place
import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = [Category::class, Partner::class, Place::class],
    version = 1,
    exportSchema = false
)
abstract class ExinityPartnersDatabase : RoomDatabase() {
    abstract fun categoryDao(): CategoryDao
    abstract fun partnerDao(): PartnerDao
    abstract fun placeDao(): PlaceDao
    abstract fun placePartnerCategoryDao(): PlacePartnerCategoryDao
}