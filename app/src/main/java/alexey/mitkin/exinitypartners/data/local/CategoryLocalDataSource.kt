package alexey.mitkin.exinitypartners.data.local

import alexey.mitkin.exinitypartners.data.CategoryDataSource
import alexey.mitkin.exinitypartners.data.Result
import alexey.mitkin.exinitypartners.data.dao.CategoryDao
import alexey.mitkin.exinitypartners.data.entity.Category
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class CategoryLocalDataSource @Inject constructor(
    private val categoryDao: CategoryDao,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : CategoryDataSource {

    override suspend fun getCategories(): Result<List<Category>> = withContext(ioDispatcher) {
        return@withContext try {
            Result.Success(categoryDao.getCategories())
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

}