package alexey.mitkin.exinitypartners.data.dao

import alexey.mitkin.exinitypartners.data.entity.PlacePartnerCategory
import androidx.room.Dao
import androidx.room.Query
import androidx.room.Transaction

@Dao
interface PlacePartnerCategoryDao {

    @Transaction
    @Query("SELECT Place.*, Partner.*, Category.* FROM Place INNER JOIN Partner ON Place.partnerId = Partner.partner_id INNER JOIN Category ON Partner.categoryId = Category.category_id WHERE Place.place_id = :placeId")
    suspend fun getPlacePartnerCategory(placeId: String): PlacePartnerCategory

    @Transaction
    @Query("SELECT Place.*, Partner.*, Category.* FROM Place INNER JOIN Partner ON Place.partnerId = Partner.partner_id INNER JOIN Category ON Partner.categoryId = Category.category_id")
    suspend fun getAllPlacePartnerCategory(): List<PlacePartnerCategory>

    @Transaction
    @Query("SELECT Place.*, Partner.*, Category.* FROM Place INNER JOIN Partner ON Place.partnerId = Partner.partner_id INNER JOIN Category ON Partner.categoryId = Category.category_id WHERE Category.category_id = :categoryId")
    suspend fun getAllPlacePartnerCategoryByCategory(categoryId: String): List<PlacePartnerCategory>

    @Transaction
    @Query("SELECT Place.*, Partner.*, Category.* FROM Place INNER JOIN Partner ON Place.partnerId = Partner.partner_id INNER JOIN Category ON Partner.categoryId = Category.category_id ORDER BY Place.place_createTs DESC LIMIT 15")
    suspend fun getRecentlyAdded(): List<PlacePartnerCategory>

}