package alexey.mitkin.exinitypartners.data.di

import alexey.mitkin.exinitypartners.data.PartnerDataSource
import alexey.mitkin.exinitypartners.data.local.ExinityPartnersDatabase
import alexey.mitkin.exinitypartners.data.local.PartnerLocalDataSource
import alexey.mitkin.exinitypartners.data.remote.PartnerRemoteDataSource
import dagger.Module
import dagger.Provides
import io.grpc.ManagedChannel
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
object PartnerModule {

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class PartnerRemoteDataSource

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class PartnerLocalDataSource

    @JvmStatic
    @Provides
    @Singleton
    @PartnerRemoteDataSource
    fun providePartnerRemoteDataSource(channel: ManagedChannel): PartnerDataSource {
        return PartnerRemoteDataSource(channel = channel)
    }

    @Provides
    @Singleton
    @PartnerLocalDataSource
    fun provideCategoryLocalDataSource(database: ExinityPartnersDatabase): PartnerDataSource {
        return PartnerLocalDataSource(partnerDao = database.partnerDao())
    }
}