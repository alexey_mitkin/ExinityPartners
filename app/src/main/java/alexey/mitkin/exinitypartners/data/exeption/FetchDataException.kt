package alexey.mitkin.exinitypartners.data.exeption

class FetchDataException : Exception("Problem data fetching")