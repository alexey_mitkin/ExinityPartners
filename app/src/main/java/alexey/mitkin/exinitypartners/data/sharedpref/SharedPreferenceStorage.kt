package alexey.mitkin.exinitypartners.data.sharedpref

import alexey.mitkin.exinitypartners.data.entity.Category
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import android.graphics.Color
import androidx.core.content.edit
import javax.inject.Inject
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class SharedPreferenceStorage @Inject constructor(context: Context) : SharedPreferenceContract {

    companion object {
        const val PREF_NAMES = "fxtm_partners_pref"

        const val IS_FIRST_OPEN_KEY = "IS_FIRST_OPEN_KEY"
        const val LAST_SAVED_AT_KEY = "LAST_UPDATED_AT_KEY"
    }

    private val prefs = context.applicationContext.getSharedPreferences(PREF_NAMES, MODE_PRIVATE)

    override var isFirstOpen: Boolean by BooleanPreference(prefs, IS_FIRST_OPEN_KEY, true)
    override var lastSavedAt: Long by LongPreference(prefs, LAST_SAVED_AT_KEY, 0)

    override fun isTheActualVersionCategoryImageSave(category: Category): Boolean {
        return if (!prefs.contains("category_image_${category.id}")) {
            false
        } else {
            prefs.getLong("category_image_${category.id}", 0) == category.updateTs
        }
    }

    override fun saveLastUpdateCategoryImage(category: Category) {
        prefs.edit().putLong("category_image_${category.id}", category.updateTs).apply()
    }

    override fun isTheActualVersionCategoryMarkerImageSave(category: Category): Boolean {
        return if (!prefs.contains("category_image_marker_${category.id}")) {
            false
        } else {
            prefs.getLong("category_image_marker_${category.id}", 0) == category.updateTs
        }
    }

    override fun saveLastUpdateCategoryMarkerImage(category: Category) {
        prefs.edit().putLong("category_image_marker_${category.id}", category.updateTs).apply()
    }

    override fun saveCategoryColor(color: Int, categoryId: String) {
        prefs.edit().putInt("${categoryId}_color", color).apply()
    }

    override fun getCategoryColor(categoryId: String?): Int {
       return prefs.getInt("${categoryId}_color", Color.BLACK)
    }

    internal class BooleanPreference(
        private val preference: SharedPreferences,
        private val name: String,
        private val defaultValue: Boolean
    ) : ReadWriteProperty<Any, Boolean> {
        override fun getValue(thisRef: Any, property: KProperty<*>): Boolean {
            return preference.getBoolean(name, defaultValue)
        }

        override fun setValue(thisRef: Any, property: KProperty<*>, value: Boolean) {
            preference.edit { putBoolean(name, value) }
        }
    }

    internal class LongPreference(
        private val preference: SharedPreferences,
        private val name: String,
        private val defaultValue: Long
    ) : ReadWriteProperty<Any, Long> {
        override fun getValue(thisRef: Any, property: KProperty<*>): Long {
            return preference.getLong(name, defaultValue)
        }

        override fun setValue(thisRef: Any, property: KProperty<*>, value: Long) {
            preference.edit { putLong(name, value) }
        }
    }
}