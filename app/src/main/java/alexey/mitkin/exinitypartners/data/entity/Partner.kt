package alexey.mitkin.exinitypartners.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.io.Serializable

@Entity
data class Partner constructor(
    @PrimaryKey @ColumnInfo(name = "partner_id") var id: String,
    @ColumnInfo(name = "partner_createTs") var createTs: Long,
    @ColumnInfo(name = "partner_createdBy") var createdBy: String,
    @ColumnInfo(name = "partner_updateTs") var updateTs: Long,
    @ColumnInfo(name = "partner_updatedBy") var updatedBy: String,
    @ColumnInfo(name = "partner_name") var name: String,
    @ColumnInfo(name = "partner_phone") var phone: String,
    @ColumnInfo(name = "partner_phoneAdditional") var phoneAdditional: String,
    @ColumnInfo(name = "partner_web") var web: String,
    @ColumnInfo(name = "partner_email") var email: String,
    @ColumnInfo(name = "categoryId") var categoryId: String
) : Serializable
