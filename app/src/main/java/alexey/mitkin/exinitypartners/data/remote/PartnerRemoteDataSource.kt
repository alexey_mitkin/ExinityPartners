package alexey.mitkin.exinitypartners.data.remote

import alexey.mitkin.exinitypartners.TIME_OUT_SECOND
import alexey.mitkin.exinitypartners.data.PartnerDataSource
import alexey.mitkin.exinitypartners.data.Result
import alexey.mitkin.exinitypartners.data.entity.Partner
import alexey.mitkin.proto.PartnerControlGrpc
import alexey.mitkin.proto.PartnerRequest
import io.grpc.ManagedChannel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class PartnerRemoteDataSource @Inject constructor(
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
    private val channel: ManagedChannel
) : PartnerDataSource {

    override suspend fun getPartners(): Result<List<Partner>> = withContext(ioDispatcher) {
        return@withContext try {
            val stub = PartnerControlGrpc.newBlockingStub(channel)
            val request = PartnerRequest.getDefaultInstance()
            val reply = stub.withDeadlineAfter(TIME_OUT_SECOND, TimeUnit.SECONDS).getPartners(request)
            val newList = reply.partnersList.map {
                Partner(
                    it.id,
                    it.createTs,
                    it.createdBy,
                    it.updateTs,
                    it.updatedBy,
                    it.name,
                    it.phone,
                    it.phoneAdditional,
                    it.web,
                    it.email,
                    it.categoryId
                )
            }
            Result.Success(newList)
        } catch (e: Exception) {
            Result.Error(e)
        }
    }
}