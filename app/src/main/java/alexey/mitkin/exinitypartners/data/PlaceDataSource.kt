package alexey.mitkin.exinitypartners.data

import alexey.mitkin.exinitypartners.data.entity.Place

interface PlaceDataSource {
    suspend fun getPlaces(): Result<List<Place>>
}