package alexey.mitkin.exinitypartners.data.di

import alexey.mitkin.exinitypartners.ImagesDataSource
import alexey.mitkin.exinitypartners.data.CategoryDataSource
import alexey.mitkin.exinitypartners.data.local.CategoryLocalDataSource
import alexey.mitkin.exinitypartners.data.local.ExinityPartnersDatabase
import alexey.mitkin.exinitypartners.data.remote.CategoryRemoteDataSource
import alexey.mitkin.exinitypartners.data.remote.ImagesRemoteDataSource
import dagger.Module
import dagger.Provides
import io.grpc.ManagedChannel
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
object CategoryModule {

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class CategoryRemoteDataSource

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class CategoryLocalDataSource

    @JvmStatic
    @Provides
    @Singleton
    @CategoryRemoteDataSource
    fun provideCategoryRemoteDataSource(channel: ManagedChannel): CategoryDataSource {
        return CategoryRemoteDataSource(channel = channel)
    }

    @JvmStatic
    @Provides
    @Singleton
    @CategoryLocalDataSource
    fun provideCategoryLocalDataSource(database: ExinityPartnersDatabase): CategoryDataSource {
        return CategoryLocalDataSource(categoryDao = database.categoryDao())
    }

    @JvmStatic
    @Provides
    @Singleton
    fun provideCategoryImagesDataSource(channel: ManagedChannel): ImagesDataSource {
        return ImagesRemoteDataSource(channel)
    }
}