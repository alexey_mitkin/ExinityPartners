package alexey.mitkin.exinitypartners.data.di

import alexey.mitkin.exinitypartners.data.PlaceDataSource
import alexey.mitkin.exinitypartners.data.local.ExinityPartnersDatabase
import alexey.mitkin.exinitypartners.data.local.PlaceLocalDataSource
import alexey.mitkin.exinitypartners.data.remote.PlaceRemoteDataSource
import dagger.Module
import dagger.Provides
import io.grpc.ManagedChannel
import javax.inject.Qualifier
import javax.inject.Singleton

@Module
object PlaceModule {

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class PlaceRemoteDataSource

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class PlaceLocalDataSource

    @JvmStatic
    @Provides
    @Singleton
    @PlaceRemoteDataSource
    fun providePlaceRemoteDataSource(channel: ManagedChannel): PlaceDataSource {
        return PlaceRemoteDataSource(channel = channel)
    }

    @Provides
    @Singleton
    @PlaceLocalDataSource
    fun providePlaceLocalDataSource(database: ExinityPartnersDatabase): PlaceDataSource {
        return PlaceLocalDataSource(placeDao = database.placeDao())
    }
}