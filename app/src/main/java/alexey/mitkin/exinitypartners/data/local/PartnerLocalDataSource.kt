package alexey.mitkin.exinitypartners.data.local

import alexey.mitkin.exinitypartners.data.PartnerDataSource
import alexey.mitkin.exinitypartners.data.Result
import alexey.mitkin.exinitypartners.data.dao.PartnerDao
import alexey.mitkin.exinitypartners.data.entity.Partner
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class PartnerLocalDataSource @Inject constructor(
    private val partnerDao: PartnerDao,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : PartnerDataSource {

    override suspend fun getPartners(): Result<List<Partner>> = withContext(ioDispatcher) {
        return@withContext try {
            Result.Success(partnerDao.getPartners())
        } catch (e: Exception) {
            Result.Error(e)
        }
    }
}