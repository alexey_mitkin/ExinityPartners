package alexey.mitkin.exinitypartners.data.sharedpref

import alexey.mitkin.exinitypartners.data.entity.Category

interface SharedPreferenceContract {
    var isFirstOpen: Boolean
    var lastSavedAt: Long

    fun isTheActualVersionCategoryImageSave(category: Category): Boolean
    fun isTheActualVersionCategoryMarkerImageSave(category: Category): Boolean
    fun saveLastUpdateCategoryImage(category: Category)
    fun saveLastUpdateCategoryMarkerImage(category: Category)
    fun saveCategoryColor(color:Int, categoryId: String)
    fun getCategoryColor(categoryId:String?):Int
}