package alexey.mitkin.exinitypartners.data.remote

import alexey.mitkin.exinitypartners.TIME_OUT_SECOND
import alexey.mitkin.exinitypartners.data.PlaceDataSource
import alexey.mitkin.exinitypartners.data.Result
import alexey.mitkin.exinitypartners.data.entity.Place
import alexey.mitkin.proto.PlaceControlGrpc
import alexey.mitkin.proto.PlaceRequest
import io.grpc.ManagedChannel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class PlaceRemoteDataSource @Inject constructor(
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO,
    private val channel: ManagedChannel
) : PlaceDataSource {

    override suspend fun getPlaces(): Result<List<Place>> = withContext(ioDispatcher) {
        return@withContext try {
            val stub = PlaceControlGrpc.newBlockingStub(channel)
            val request = PlaceRequest.getDefaultInstance()
            val reply = stub.withDeadlineAfter(TIME_OUT_SECOND, TimeUnit.SECONDS).getPlaces(request)
            val newList = reply.placesList.map {
                Place(
                    id = it.id,
                    createTs = it.createTs,
                    createdBy = it.createdBy,
                    updateTs = it.updateTs,
                    updatedBy = it.updatedBy,
                    name = it.name,
                    address = it.address,
                    partnerId = it.partnerId,
                    discountRate = it.discountRate,
                    coordinates = it.coordinates,
                    monday = it.monday,
                    tuesday = it.tuesday,
                    wednesday = it.wednesday,
                    thursday = it.thursday,
                    friday = it.friday,
                    saturday = it.saturday,
                    sunday = it.sunday,
                    isCardRequired = it.cardRequired,
                    email = it.email,
                    phone = it.phone,
                    phoneAdditional = it.phoneAdditional,
                    web = it.web
                )
            }
            Result.Success(newList)
        } catch (e: Exception) {
            Result.Error(e)
        }
    }
}
