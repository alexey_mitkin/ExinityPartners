package alexey.mitkin.exinitypartners.data.dao

import alexey.mitkin.exinitypartners.data.entity.Place
import androidx.room.*

@Dao
interface PlaceDao {
    @Query("SELECT * FROM Place")
    suspend fun getPlaces(): List<Place>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPlace(place: Place)

    @Transaction
    @Query("SELECT Place.* FROM Place INNER JOIN Partner ON Place.partnerId = Partner.partner_id INNER JOIN Category ON Partner.categoryId = Category.category_id WHERE category_id = :categoryId")
    suspend fun getAllPlacesByCategory(categoryId: String): List<Place>

    @Query("DELETE FROM Place")
    suspend fun deleteAll()
}