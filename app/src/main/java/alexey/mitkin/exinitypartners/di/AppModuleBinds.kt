package alexey.mitkin.exinitypartners.di

import alexey.mitkin.exinitypartners.AppRepository
import alexey.mitkin.exinitypartners.DefaultAppRepository
import alexey.mitkin.exinitypartners.data.sharedpref.SharedPreferenceContract
import alexey.mitkin.exinitypartners.data.sharedpref.SharedPreferenceStorage
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class AppModuleBinds {

    @Singleton
    @Binds
    abstract fun bindRepository(repo: DefaultAppRepository): AppRepository

    @Singleton
    @Binds
    abstract fun bindSharedPreference(preference: SharedPreferenceStorage): SharedPreferenceContract
}