package alexey.mitkin.exinitypartners.di


import alexey.mitkin.exinitypartners.di.anotation.ActivityScoped
import alexey.mitkin.exinitypartners.ui.blank.BlankModule
import alexey.mitkin.exinitypartners.ui.launcher.MainActivity
import alexey.mitkin.exinitypartners.ui.launcher.MainActivityModule
import alexey.mitkin.exinitypartners.ui.loading.LoadingModule
import alexey.mitkin.exinitypartners.ui.map.MapModule
import alexey.mitkin.exinitypartners.ui.menu.MenuModule
import alexey.mitkin.exinitypartners.ui.offline.OfflineModule
import alexey.mitkin.exinitypartners.ui.onboarding.OnboardingModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(
        modules = [
            MainActivityModule::class,
            BlankModule::class,
            OnboardingModule::class,
            MenuModule::class,
            MapModule::class,
            LoadingModule::class,
            OfflineModule::class
        ]
    )
    internal abstract fun launcherActivity(): MainActivity
}