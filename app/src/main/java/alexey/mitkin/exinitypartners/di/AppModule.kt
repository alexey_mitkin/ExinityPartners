package alexey.mitkin.exinitypartners.di

import alexey.mitkin.exinitypartners.ENDPOINT
import alexey.mitkin.exinitypartners.data.local.ExinityPartnersDatabase
import android.content.Context
import androidx.room.Room
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import dagger.Module
import dagger.Provides
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import kotlinx.coroutines.Dispatchers
import javax.inject.Singleton

@Module
object AppModule {

    @JvmStatic
    @Singleton
    @Provides
    fun provideIoDispatcher() = Dispatchers.IO

    @JvmStatic
    @Singleton
    @Provides
    fun provideDataBase(context: Context): ExinityPartnersDatabase {
        return Room.databaseBuilder(
            context,
            ExinityPartnersDatabase::class.java,
            "Exinity.db"
        ).build()
    }

    @JvmStatic
    @Singleton
    @Provides
    fun provideChanel(): ManagedChannel = ManagedChannelBuilder
        .forTarget(ENDPOINT)
        .usePlaintext()
        .build()

    @JvmStatic
    @Singleton
    @Provides
    fun provideFusedLocationProviderClient(context: Context): FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(context)

}