package alexey.mitkin.exinitypartners.di

import alexey.mitkin.exinitypartners.PlacesApplication
import alexey.mitkin.exinitypartners.data.di.CategoryModule
import alexey.mitkin.exinitypartners.data.di.PartnerModule
import alexey.mitkin.exinitypartners.data.di.PlaceModule
import alexey.mitkin.exinitypartners.di.data.ViewModelModule
import android.content.Context
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityBindingModule::class,
        AppModule::class,
        AppModuleBinds::class,
        ViewModelModule::class,
        CategoryModule::class,
        PartnerModule::class,
        PlaceModule::class
    ]
)
interface AppComponent : AndroidInjector<PlacesApplication> {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Context): AppComponent
    }
}