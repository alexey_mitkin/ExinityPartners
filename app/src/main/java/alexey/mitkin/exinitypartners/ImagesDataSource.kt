package alexey.mitkin.exinitypartners

import com.google.protobuf.ByteString

interface ImagesDataSource {
    suspend fun getImageBytesById(categoryId:String):ByteString
}